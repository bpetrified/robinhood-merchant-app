module.exports = {
    preset: 'react-native',
    moduleFileExtensions: [
        "ts",
        "tsx",
        "js",
        "jsx",
        "json",
        "node"
    ],
    transformIgnorePatterns: [
        "/node_modules/(?!react-native|native-base-shoutem-theme|react-navigation-stack|@react-navigation|react-navigation-fluid-transitions|@react-native-community)"
    ],
    testPathIgnorePatterns: [
        "/node_modules/"
    ],
    setupFiles: [
        './test-setup.js'
    ],
    testResultsProcessor: "./node_modules/jest-junit-reporter",
    coverageReporters: [
        "text",
        "cobertura",
        "lcov",
        "json",
        "clover"
    ],
    reporters: [
        "default",
        "jest-junit"
    ]
};