import { combineReducers } from 'redux';
import { MainState, mainReducers } from './main/main-reducer';
import { HttpState, httpReducers } from './shared/features/http/http-reducer';

export const rootReducer = combineReducers<RootState>({
    main: mainReducers,
    http: httpReducers
});

export type RootState = {
    main: MainState,
    http: HttpState
};
