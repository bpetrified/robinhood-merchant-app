import React from 'react';
import { Text, TextProps } from 'react-native';

export const RBHText: React.FC<TextProps & { children: any }> = (props) => {
    return <Text {...props}>{props.children}</Text>;
};