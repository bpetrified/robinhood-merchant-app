import { useFocusEffect, useNavigation } from '@react-navigation/native';
import { StackNavigationOptions } from '@react-navigation/stack';
import { useCallback, useEffect } from 'react';
import { BackHandler } from 'react-native';

export const useNavigationSetOption = (options: StackNavigationOptions) => {
    const navigation = useNavigation();
    useEffect(() => {
        navigation.setOptions(options);
    }, [navigation]);
};

export const useOnScreenLifeCycle = (onFocus: () => void, onUnFocus: () => void) => {
    useFocusEffect(
        useCallback(() => {
            onFocus();
            return () => {
                onUnFocus();
            };
        }, [])
    );
};

/** 
 * To override default android back button press handler by react-navigation.
 * The handler must return true/false. If the handler return true, default react-navigation
 * behavior will be skipped.
 * @param handler to be executed when back button is pressed
 */
export const useOnAndroidBack = (handler: () => boolean) => {
    useOnScreenLifeCycle(() => {
        BackHandler.addEventListener('hardwareBackPress', handler);
    }, () => {
        BackHandler.removeEventListener('hardwareBackPress', handler);
    });
};
