import React from 'react';
import { createStackNavigator, TransitionPresets } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { NavigationHelper } from '../navigation-helper';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { RBHText } from '../../../components/rbh-text/rbh-text';
import { MAIN_SCREEN_LIST } from '../../../../main/main-constant';
import { InitialScreen } from '../../../../main/screens/initial';
import { DemoScreen } from '../../../../main/screens/demo';

const Stack = createStackNavigator<MainStackParamList>();
const screenOptions = () => {
    return {
        ...TransitionPresets.SlideFromRightIOS,
        headerLeft: () => <TouchableHighlight onPress={NavigationHelper.goBack}><RBHText>Back</RBHText></TouchableHighlight>
    };
};

export const RBHNavigationContainer = () => {
    return (<NavigationContainer ref={NavigationHelper.navigationRef}>
        <Stack.Navigator initialRouteName={MAIN_SCREEN_LIST.INITIAL} screenOptions={screenOptions}>
            <Stack.Screen name={MAIN_SCREEN_LIST.INITIAL} component={InitialScreen} />
            <Stack.Screen name={'Demo'} component={DemoScreen} />
        </Stack.Navigator>
    </NavigationContainer>);
};


export type MainStackParamList = {
    [MAIN_SCREEN_LIST.INITIAL]: {};
    Demo: { userId: string };
};