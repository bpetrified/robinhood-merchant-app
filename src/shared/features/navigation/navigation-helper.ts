import { NavigationContainerRef, CommonActions } from '@react-navigation/native';
import React from 'react';
import { MainStackParamList } from './components/navigation-container';

class NavigationHelperClass {
    navigationRef = React.createRef<NavigationContainerRef>();

    /**
     * Navigate to a route in current navigation tree.
     *
     * @param name Name of the route to navigate to.
     * @param [params] Params object for the route.
     */
    navigate = <ScreenName extends keyof MainStackParamList>(screen: ScreenName, params?: MainStackParamList[ScreenName]) => {
        this.navigationRef.current?.navigate(screen, params);
    }

    /**
     * Go back to the previous route in history.
     */
    goBack = () => {
        this.navigationRef.current?.goBack();
    }

    /**
     * Reset all navigation tree in stack and immediatly pop to the screen with params
     */
    reset = <ScreenName extends keyof MainStackParamList>(screen: ScreenName, params?: MainStackParamList[ScreenName]) => {
        this.navigationRef.current?.dispatch(CommonActions.reset({
            index: 0,
            routes: [
                { name: screen, params },
            ]
        }));
    }
}

export const NavigationHelper = new NavigationHelperClass();