import _ from 'lodash';
import { AxiosResponse } from 'axios';

export class HttpResponse {
  status: number;
  success: boolean;
  data: any;
  raw: any;

  constructor(raw: AxiosResponse<any>) {
    this.raw = raw;
    this.status = _.get(raw, 'status');
    this.success = this.status === 200;
    this.data = this.success ? _.get(raw, 'data') : undefined;
  }
}
