import axios, { AxiosInstance, AxiosRequestConfig, CancelTokenSource } from 'axios';
import { HttpResponse } from './models/http-response';

class HttpHelperClass {
  private instance: AxiosInstance | undefined;
  private currentCalls = {} as any;
  private cancelTokens = {} as { [key: string]: CancelTokenSource };

  /**
   * Check if instance has been created otherwise throw error
   */
  private validateInstance = () => {
    (!this.instance ? () => {
      throw new Error('Error!! Instance has not been created. Please call "createInstance" first');
    } : () => {
    })();
  }

  /**
   * Create instance with default configs
   * @param config default configs
   */
  createInstance = (config: AxiosRequestConfig) => {
    this.instance = axios.create(config);
  }

  /**
   * Set call task to be used by saga in cancellation mechanism.
   *
   * @param id http request identifier
   * @param task saga task
   */
  setCallTask = (id: string, task: any) => {
    this.currentCalls[id] = task;
  }

  /**
   * Get call task reference 
   *
   * @param id http request identifier
   */
  getCallTask = (id: string) => {
    return this.currentCalls[id];
  }

  /**
   * Delete call task reference after call completed or cancelled
   *
   * @param id http request identifier
   */
  deleteCallTask = (id: string) => {
    delete this.currentCalls[id];
  }

  /**
   * Delete cancel token after the call is completed or cancelled
   *
   * @param id http request identifier
   */
  deleteCancelToken = (id: string) => {
    delete this.cancelTokens[id];
  }

  /**
   * Cancel axios instance http call
   */
  cancel = (id: string) => {
      (this.cancelTokens[id] ? () => { this.cancelTokens[id].cancel(); } : () => {})();
      this.deleteCancelToken(id);
  }

  /**
   * Set http header that will be applied to all request coming from default instance.
   *
   * @param key http header key
   * @param value 
   */
  setHeader = (key: string, value: any) => {
    this.validateInstance();
    if (this.instance) {
      Object.assign(this.instance.defaults, {
        headers: {
          ...this.instance.defaults.headers,
          [key]: value
        }
      });
    }
  }

  /**
   * Fire http get request and catch error, no need to use try/catch when call this method
   *
   * @param param get request param
   */
  get = async ({ url, config = {}, id }: HttpGetParams): Promise<HttpResponse> => {
    this.validateInstance();
    try {
      const CancelToken = axios.CancelToken;
      (id ? () => { this.cancelTokens[id] = CancelToken.source(); } : () => { })();
      const resp = await (this.instance as AxiosInstance).get(url, { ...config, cancelToken: id ? this.cancelTokens[id].token : undefined });
      return new HttpResponse(resp);
    } catch (error) {
      return  new HttpResponse(error);
    }
  }

  /**
   * Fire http post request and catch error, no need to use try/catch when call this method
   *
   * @param param post request param
   */
  post = async ({ url, data, config, id }: HttpPostParams): Promise<HttpResponse> => {
    this.validateInstance();
    try {
      const CancelToken = axios.CancelToken;
      (id ? () => { this.cancelTokens[id] = CancelToken.source(); } : () => { })();
      const resp = await (this.instance as AxiosInstance).post(url, data, { ...config, cancelToken: id ? this.cancelTokens[id].token : undefined });
      return new HttpResponse(resp);
    } catch (error) {
      return  new HttpResponse(error);
    }
  }
}

export const HttpHelper = new HttpHelperClass();

export type HttpGetParams = {
  url: string;
  config?: AxiosRequestConfig;
  id?: string; // For saving cancel token
};

export type HttpPostParams = HttpGetParams & { data: any };