import { RootState } from '../../../reducer';
import { HttpActions, HTTP_ACTION_TYPE } from './http-type';
import { useSelector } from 'react-redux';

export enum HTTP_STATE_KEY {
    IS_LOADING = 'isLoading',
    IS_FAILED = 'isFailed'
}

export type HttpState = {
    [HTTP_STATE_KEY.IS_LOADING]: { [scope: string]: boolean };
    [HTTP_STATE_KEY.IS_FAILED]: { [scope: string]: boolean };
};

export const INITIAL_HTTP_STATE = {
    [HTTP_STATE_KEY.IS_LOADING]: {},
    [HTTP_STATE_KEY.IS_FAILED]: {}
};

export const httpReducers = (state: HttpState = INITIAL_HTTP_STATE, action: HttpActions) => {
    switch (action.type) {
        case HTTP_ACTION_TYPE.SET_LOADING:
        case HTTP_ACTION_TYPE.SET_FAILED:
            const key = action.type === HTTP_ACTION_TYPE.SET_LOADING ? HTTP_STATE_KEY.IS_LOADING : HTTP_STATE_KEY.IS_FAILED;
            const { id, result } = action.payload;
            const _newState = { ...state,
                 [key]: { ...state[key], [id]: result } } as any;
            if (!result) {
                delete _newState[key][id];
            }
            return _newState;
        default:
            return state;
    }
};

export const useLoadingSelector =  (id: string) => useSelector((state: RootState) => state.http['isLoading'][id]);
export const useRequestFailedSelector =  (id: string) => useSelector((state: RootState) => state.http['isFailed'][id]);
