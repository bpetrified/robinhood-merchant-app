import { call, cancel, fork, join, put, takeEvery } from 'redux-saga/effects';
import { httpCreators } from './http-action';
import { HttpHelper } from './http-helper';
import { HttpCancelAction, HttpRequestAction, HTTP_ACTION_TYPE } from './http-type';
import { HttpResponse } from './models/http-response';

export type SagaHttpHandler = (resp: HttpResponse, callbackParams: any) => Generator<never, void, unknown>;

function* request(action: HttpRequestAction) {
    const { sagaHandler, callbackParams, id, serviceParams, service } = action.payload;
    // Random id if not provided
    const _id = id || (yield call(getRandomString));
    // Cancel current task with the same id
    yield call(cancelRequest, httpCreators.cancel(_id));
    // Request is going to start, set loading to true
    yield put(httpCreators.setLoading(_id, true));
    // Execute service call by "Fork" task
    const task = yield (fork as any)(service, serviceParams, id);
    // Save task ref for cancellation mechanism
    yield call(HttpHelper.setCallTask, _id, task);
    // Wait for task to resolve
    const resp = yield join(task);
    // Set loading to false
    yield put(httpCreators.setLoading(_id, false));
    // Delete call task ref
    yield call(HttpHelper.deleteCallTask, _id);
    // Set failed result
    yield put(httpCreators.setFailed(_id, !resp.success));
    // Forward to handler
    yield call(sagaHandler ? sagaHandler : (() => { }) as any, resp, callbackParams);
}

function* cancelRequest(action: HttpCancelAction) {
    const id = action.payload;
    // Cancel axios request
    yield call(HttpHelper.cancel, id);
    // Cancel saga task
    yield cancel(HttpHelper.getCallTask(id));
    // Remove task ref
    yield call(HttpHelper.deleteCallTask, id);
    // Clear loading, failed flags
    yield put(httpCreators.setLoading(id, false));
    yield put(httpCreators.setFailed(id, false));
}

function getRandomString() {
    return `${Math.random().toString(36).substr(2, 5)}${new Date().getTime().toString()}`;
}

export const httpSagas = {
    request,
    cancelRequest,
    getRandomString,
    watchers: [
        takeEvery(HTTP_ACTION_TYPE.REQUEST, request),
        takeEvery(HTTP_ACTION_TYPE.CANCEL, cancelRequest)
    ]
};