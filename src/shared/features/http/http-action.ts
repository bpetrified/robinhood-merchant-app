import { HttpCancelAction, HttpRequestAction, HttpRequestActionParams, HttpSetFailedAction, HttpSetLoadingAction, HTTP_ACTION_TYPE } from './http-type';

export const httpCreators = {
    request: (params: HttpRequestActionParams): HttpRequestAction => ({
        type: HTTP_ACTION_TYPE.REQUEST,
        payload: params
    }),
    cancel: (id: string): HttpCancelAction => ({
        type: HTTP_ACTION_TYPE.CANCEL,
        payload: id
    }),
    setLoading: (id: string, result: boolean): HttpSetLoadingAction => ({
        type: HTTP_ACTION_TYPE.SET_LOADING,
        payload: { id, result }
    }),
    setFailed: (id: string, result: boolean): HttpSetFailedAction => ({
        type: HTTP_ACTION_TYPE.SET_FAILED,
        payload: { id, result }
    })
};
