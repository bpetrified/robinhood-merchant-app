import { SagaHttpHandler } from './http-saga';
import { HttpResponse } from './models/http-response';

export enum HTTP_ACTION_TYPE {
    REQUEST = 'HTTP_REQUEST',
    CANCEL = 'HTTP_CANCEL',
    POST = 'HTTP_POST',
    SET_LOADING = 'HTTP_SET_LOADING',
    SET_FAILED = 'HTTP_SET_FAILED'
}

export type HttpRequestAction = {
    type: HTTP_ACTION_TYPE.REQUEST;
    payload: HttpRequestActionParams;
};

export type HttpCancelAction = {
    type: HTTP_ACTION_TYPE.CANCEL;
    payload: string;
};

export type HttpSetLoadingAction = {
    type: HTTP_ACTION_TYPE.SET_LOADING;
    payload: { id: string, result: boolean };
};

export type HttpSetFailedAction = {
    type: HTTP_ACTION_TYPE.SET_FAILED;
    payload: { id: string, result: boolean };
};

export type HttpActions = HttpRequestAction | HttpSetLoadingAction | HttpSetFailedAction | HttpCancelAction;

export type HttpRequestActionParams = {
    id?: string, // if this is undefined, cancellation and loading status monitor will not be available
    sagaHandler?: SagaHttpHandler // Saga worker that will be called when the request is finish
    callbackParams?: any;
    service: HttpService;
    serviceParams?: any;
};

export type HttpService = (params: { id: string }) => Promise<HttpResponse>;