import { StyleSheet } from 'react-native';
import metrics from './metric';

export const commonStyles = StyleSheet.create({
    flex: {
        flex: 1
    },
    halfFlex: {
        flex: 0.5
    },
    spaceBetween: {
        justifyContent: 'space-between'
    },
    spaceAround: {
        justifyContent: 'space-around'
    },
    row: {
        flexDirection: 'row'
    },
    col: {
        flexDirection: 'column'
    },
    textLeft: {
        textAlign: 'left'
    },
    textRight: {
        textAlign: 'right'
    },
    textCenter: {
        textAlign: 'center'
    },
    margin: {
        margin: metrics.baseMargin
    },
    marginLarge: {
        margin: metrics.baseMargin * 1.5
    },
    marginVert: {
        marginVertical: metrics.baseMargin
    },
    marginVertLarge: {
        marginVertical: metrics.baseMargin * 1.5
    },
    marginHorz: {
        marginHorizontal: metrics.baseMargin
    },
    marginHorzLarge: {
        marginHorizontal: metrics.baseMargin * 1.5
    },
    marginLeft: {
        marginLeft: metrics.baseMargin
    },
    marginRight: {
        marginRight: metrics.baseMargin
    },
    marginTop: {
        marginTop: metrics.baseMargin
    },
    marginTopLarge: {
        marginTop: metrics.baseMargin * 1.5
    },
    marginBottom: {
        marginBottom: metrics.baseMargin
    },
    marginBottomLarge: {
        marginBottom: metrics.baseMargin * 1.5
    },
    marginSmall: {
        margin: metrics.baseMargin / 2
    },
    marginVertSmall: {
        marginVertical: metrics.baseMargin / 2
    },
    marginHorzSmall: {
        marginHorizontal: metrics.baseMargin / 2
    },
    marginLeftSmall: {
        marginLeft: metrics.baseMargin / 2
    },
    marginRightSmall: {
        marginRight: metrics.baseMargin / 2
    },
    marginTopSmall: {
        marginTop: metrics.baseMargin / 2
    },
    marginBottomSmall: {
        marginBottom: metrics.baseMargin / 2
    },
    padding: {
        padding: metrics.baseMargin
    },
    paddingTopLarge: {
        paddingTop: metrics.baseMargin * 1.5
    },
    paddingLarge: {
        padding: metrics.baseMargin * 1.5
    },
    paddingVertLarge: {
        paddingVertical: metrics.baseMargin * 1.5
    },
    paddingHorzLarge: {
        paddingHorizontal: metrics.baseMargin * 1.5
    },
    paddingXLarge: {
        padding: metrics.baseMargin * 2
    },
    paddingVertXLarge: {
        paddingVertical: metrics.baseMargin * 2
    },
    paddingHorzXLarge: {
        paddingHorizontal: metrics.baseMargin * 2
    },
    paddingTopXLarge: {
        paddingTop: metrics.baseMargin * 2
    },
    paddingBottomXLarge: {
        paddingBottom: metrics.baseMargin * 2
    },
    paddingLeftXLarge: {
        paddingLeft: metrics.baseMargin * 2
    },
    paddingRightXLarge: {
        paddingRight: metrics.baseMargin * 2
    },
    paddingXXLarge: {
        padding: metrics.baseMargin * 3
    },
    paddingVertXXLarge: {
        paddingVertical: metrics.baseMargin * 3
    },
    paddingHorzXXLarge: {
        paddingHorizontal: metrics.baseMargin * 3
    },
    paddingTopXXLarge: {
        paddingTop: metrics.baseMargin * 3
    },
    paddingBottomXXLarge: {
        paddingBottom: metrics.baseMargin * 3
    },
    paddingLeftXXLarge: {
        paddingLeft: metrics.baseMargin * 3
    },
    paddingRightXXLarge: {
        paddingRight: metrics.baseMargin * 3
    },
    paddingVert: {
        paddingVertical: metrics.baseMargin
    },
    paddingHorz: {
        paddingHorizontal: metrics.baseMargin
    },
    paddingTop: {
        paddingTop: metrics.baseMargin
    },
    paddingBottom: {
        paddingBottom: metrics.baseMargin
    },
    paddingLeft: {
        paddingLeft: metrics.baseMargin
    },
    paddingRight: {
        paddingRight: metrics.baseMargin
    },
    paddingSmall: {
        padding: metrics.baseMargin / 2
    },
    paddingVertSmall: {
        paddingVertical: metrics.baseMargin / 2
    },
    paddingHorzSmall: {
        paddingHorizontal: metrics.baseMargin / 2
    },
    paddingTopSmall: {
        paddingTop: metrics.baseMargin / 2
    },
    paddingBottomSmall: {
        paddingBottom: metrics.baseMargin / 2
    },
    paddingLeftSmall: {
        paddingLeft: metrics.baseMargin / 2
    },
    paddingRightSmall: {
        paddingRight: metrics.baseMargin / 2
    }
});
