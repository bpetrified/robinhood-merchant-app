import { Dimensions } from 'react-native';
import { moderateScale } from 'react-native-size-matters';

const { width, height } = Dimensions.get('window');

export const metrics = {
  baseMargin: moderateScale(16),
  screenWidth: width < height ? width : height,
  screenHeight: width < height ? height : width,
};

export default metrics;