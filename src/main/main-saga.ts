import { takeLatest } from 'redux-saga/effects';
import { HttpResponse } from '../shared/features/http/models/http-response';
import { MAIN_ACTION_TYPE, TestSagaAction } from './main-type';

function* test(action: TestSagaAction) {
    console.log('payload=', );
 }

 function* testHttpHandler(resp: HttpResponse, callbackParams: any)  {
     console.log('resp=', resp);
     console.log('params=', callbackParams);
 }

export const mainSagas = {
    test,
    testHttpHandler,
    watchers: [
        takeLatest(MAIN_ACTION_TYPE.TEST_SAGA, test)
    ]
};
