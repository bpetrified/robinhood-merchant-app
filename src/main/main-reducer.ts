import { MainActions, MAIN_ACTION_TYPE } from './main-type';
import { RootState } from '../reducer';

export enum MAIN_STATE_KEY {
    TEST_KEY_1 = 'testKey1',
    TEST_KEY_2 = 'testKey2',
}

export type MainState = {
    [MAIN_STATE_KEY.TEST_KEY_1]?: boolean,
    [MAIN_STATE_KEY.TEST_KEY_2]?: string
};

const initialState: MainState = {
};

export const mainReducers = (state = initialState, action: MainActions) => {
    switch (action.type) {
        case MAIN_ACTION_TYPE.SET_MAIN_STATE:
            return {
                ...state,
                [action.payload.key]: action.payload.value
            };
        default:
            return state;
    }
};

export const mainStateSelector = (key: MAIN_STATE_KEY) => (state: RootState) => state.main[key];