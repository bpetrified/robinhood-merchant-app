import React from 'react';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import { RBHNavigationContainer } from '../../shared/features/navigation/components/navigation-container';
import { store } from '../../store';

export const App = () => {
  return (
    <Provider store={store}>
      {/* <NavigationContainer ref={NavigationHelper.navigationRef}>
        <Stack.Navigator initialRouteName={MAIN_SCREEN_LIST.INITIAL} screenOptions={screenOptions}>
          <Stack.Screen name={MAIN_SCREEN_LIST.INITIAL} component={InitialScreen} />
          <Stack.Screen name={'Demo'} component={DemoScreen} />
        </Stack.Navigator>
      </NavigationContainer> */}
      <RBHNavigationContainer/>
    </Provider>
  );
};
