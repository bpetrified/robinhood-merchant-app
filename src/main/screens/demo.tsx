import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React from 'react';
import { View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { RBHText } from '../../shared/components/rbh-text/rbh-text';
import { httpCreators } from '../../shared/features/http/http-action';
import { useLoadingSelector, useRequestFailedSelector } from '../../shared/features/http/http-reducer';
import { MainStackParamList } from '../../shared/features/navigation/components/navigation-container';
import { NavigationHelper } from '../../shared/features/navigation/navigation-helper';
import { useNavigationSetOption, useOnAndroidBack, useOnScreenLifeCycle } from '../../shared/features/navigation/navigation-hook';
import { commonStyles } from '../../shared/themes/common-style';
import { mainCreators } from '../main-action';
import { MAIN_REQUEST_ID_LIST, MAIN_SCREEN_LIST } from '../main-constant';
import { mainStateSelector, MAIN_STATE_KEY } from '../main-reducer';
import { mainSagas } from '../main-saga';
import { MainService } from '../main-service';

export const DemoScreen: React.FC<Props> = ({ navigation, route }) => {
    // We can set navigation option dynamically in screen with hook.
    useNavigationSetOption({ title: 'hhhhhh' });

    // Focus, blur event
    useOnScreenLifeCycle(() => { console.log('focusss 2'); }, () => { console.log('blurrrr 2'); });

    // Handling back button
    useOnAndroidBack(() => {
        console.log('back press 2');
        return false;
    });

    // Dispatch is now coming from hook.
    const dispatch = useDispatch();

    // Get state by hook
    const value = useSelector(mainStateSelector(MAIN_STATE_KEY.TEST_KEY_2));
    // Access Http request status
    const isLoading = useLoadingSelector(MAIN_REQUEST_ID_LIST.GET_VERSION);
    const isFailed = useRequestFailedSelector(MAIN_REQUEST_ID_LIST.GET_VERSION);

    return (
        // Reusable style in "common" style
        <View style={commonStyles.flex}>
            {/* Navigation will be done by "Navigation Helper" */}
            <TouchableHighlight onPress={() => NavigationHelper.reset(MAIN_SCREEN_LIST.INITIAL)}>
                <RBHText>{route.params.userId}</RBHText>
            </TouchableHighlight>
            {/* Action creator is now type safe which mean key/value types must match */}
            <TouchableHighlight onPress={() => dispatch(mainCreators.setMainState(MAIN_STATE_KEY.TEST_KEY_2, '12345'))}>
                <RBHText>{'setting some key in state to "12345"'} {value}</RBHText>
            </TouchableHighlight>
            {/* Trigger saga */}
            <TouchableHighlight onPress={() => dispatch(mainCreators.testSaga('datassss'))}>
                <RBHText>{'testaction'}</RBHText>
            </TouchableHighlight>
            {/* Trigger http call */}
            <TouchableHighlight onPress={() => {
                dispatch(httpCreators.request({
                    id: MAIN_REQUEST_ID_LIST.GET_VERSION,
                    service: MainService.getVersion,
                    sagaHandler: mainSagas.testHttpHandler,
                    serviceParams: '1234',
                    callbackParams: '5678'
                }));
                // dispatch(httpCreators.cancel(MAIN_REQUEST_ID_LIST.GET_VERSION));
            }}>
                <View>
                    <RBHText>{'http call'} {'loading= ' + `${!!isLoading ? 'true' : 'false'}`}</RBHText>
                    <RBHText>{'http call'} {'failed= ' + `${!!isFailed ? 'true' : 'false'}`}</RBHText>
                </View>
            </TouchableHighlight>
        </View>);
};


type DemoScreenRouteProp = RouteProp<MainStackParamList, 'Demo'>;

type DemoScreenNavigationProp = StackNavigationProp<
    MainStackParamList,
    'Demo'
>;

type Props = {
    route: DemoScreenRouteProp;
    navigation: DemoScreenNavigationProp;
};