import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import React, { useEffect } from 'react';
import { View } from 'react-native';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { RBHText } from '../../shared/components/rbh-text/rbh-text';
import { MainStackParamList } from '../../shared/features/navigation/components/navigation-container';
import { NavigationHelper } from '../../shared/features/navigation/navigation-helper';
import { MAIN_SCREEN_LIST } from '../main-constant';
import { HttpHelper } from '../../shared/features/http/http-helper';

export const InitialScreen: React.FC<Props> = ({ navigation, route }) => {    

    useEffect(() => {
        HttpHelper.createInstance({ baseURL: 'https://merchantbff-dev.se.scb.co.th'});
    }, []);

    return (<View style={{ flex: 1, backgroundColor: 'red' }}>
        <TouchableHighlight onPress={() => NavigationHelper.navigate('Demo', { userId: 'sss' })}>
            <RBHText>helloo</RBHText>
        </TouchableHighlight>
    </View>);
};

type InitialScreenRouteProp = RouteProp<MainStackParamList, MAIN_SCREEN_LIST.INITIAL>;

type InitialScreenNavigationProp = StackNavigationProp<MainStackParamList, MAIN_SCREEN_LIST.INITIAL>;

type Props = {
    route: InitialScreenRouteProp;
    navigation: InitialScreenNavigationProp;
};