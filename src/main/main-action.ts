import { MainState } from './main-reducer';
import { MainActions, MAIN_ACTION_TYPE } from './main-type';

export const mainCreators = {
    setMainState: <KeyName extends keyof MainState>(key: KeyName, value: MainState[KeyName])
        : MainActions => ({
        type: MAIN_ACTION_TYPE.SET_MAIN_STATE,
        payload: { key, value }
    }),
    testSaga: (data: any): MainActions => ({
        type: MAIN_ACTION_TYPE.TEST_SAGA,
        payload: { data }
    })
};
