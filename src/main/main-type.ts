import { MAIN_STATE_KEY, MainState } from './main-reducer';

export enum MAIN_ACTION_TYPE {
    SET_MAIN_STATE = 'SET_MAIN_STATE',
    TEST_SAGA = 'TEST_SAGA'
}

export type SetMainStateAction<KeyName extends keyof MainState> = {
    type: MAIN_ACTION_TYPE.SET_MAIN_STATE;
    payload: { key: KeyName, value: MainState[KeyName] };
};

export type TestSagaAction = {
    type: MAIN_ACTION_TYPE.TEST_SAGA;
    payload: { data: any };
};

export type MainActions = SetMainStateAction<MAIN_STATE_KEY> | TestSagaAction;