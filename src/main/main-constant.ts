export enum MAIN_SCREEN_LIST {
    INITIAL = 'initialScreen'
}

export enum MAIN_REQUEST_ID_LIST {
    GET_VERSION = 'GET_VERSION'
}
