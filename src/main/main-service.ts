import { HttpHelper } from '../shared/features/http/http-helper';
import { MAIN_REQUEST_ID_LIST } from './main-constant';

class MainServiceClass {

    getVersion = async (params: any) => {
        return await HttpHelper.get({ url: 'https://merchantbff-dev.se.scb.co.t', id: MAIN_REQUEST_ID_LIST.GET_VERSION });
    }
}

export const MainService = new MainServiceClass();