import { all } from 'redux-saga/effects';
import { httpSagas } from './shared/features/http/http-saga';
import { mainSagas } from './main/main-saga';

export function* rootSagas() {
    yield all([
        ...mainSagas.watchers,
        ...httpSagas.watchers
    ]);
}